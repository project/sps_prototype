#!/bin/sh

DRUSH_OPTS='--working-copy'
MAKEFILE='build-sps_prototype.make'
TARGET=$1
CALLPATH=`dirname $0`
drush make $DRUSH_OPTS $CALLPATH/$MAKEFILE $TARGET

