<?php

function sps_prototype_create_article_fields() {
  $field = array(
    'field_name' => 'field_image',
    'type' => 'image',
  );
  field_create_field($field);


  // Many of the following values will be defaulted, they're included here as an illustrative examples.
  // See http://api.drupal.org/api/function/field_create_instance/7
  $instance = array(
    'field_name' => 'field_image',
    'entity_type' => 'node',
    'label' => 'Image',
    'bundle' => 'article',
    'description' => st('Upload an image to go with this article.'),

    'settings' => array(
      'file_directory' => 'field/image',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'alt_field' => TRUE,
      'title_field' => '',
    ),

    'widget' => array(
      'type' => 'image_image',
      'settings' => array(
        'progress_indicator' => 'throbber',
        'preview_image_style' => 'thumbnail',
      ),
      'weight' => -1,
    ),

    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'type' => 'image',
        'settings' => array('image_style' => 'large', 'image_link' => ''),
        'weight' => -1,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'type' => 'image',
        'settings' => array('image_style' => 'medium', 'image_link' => 'content'),
        'weight' => -1,
      ),
    ),
  );
  field_create_instance($instance);

  $field = array(
    'field_name' => 'field_collection',
    'type' => 'collection',
  );
  field_create_field($field);

  $instance = array(
    'bundle' => 'article',
    'entity_type' => 'node',
    'field_name' => 'field_collection',
    'label' => 'Collection',
    'widget' => array(
      'module' => 'collections',
      'type' => 'collection',
    ),
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
      ),
      'teaser' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
      ),
    ),
  );
  field_create_instance($instance);
}

function sps_prototype_create_collections() {
  $values = array(
    'label' => 'Test Collecction 1',
    'name' => 'test_collection_1',
  );

  $collection = collection_create($values);
  $collection->save();

  $values = array(
    'label' => 'Test Collecction 2',
    'name' => 'test_collection_2',
  );

  $collection = collection_create($values);
  $collection->save();
}

function sps_prototype_block_view() {
  $view = new view;
  $view->name = 'article_list';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'article list';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'article list';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '5';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['row_plugin'] = 'node';
  $handler->display->display_options['row_options']['links'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['link_to_node'] = 1;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'article' => 'article',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');

  views_save_view($view);
}

function sps_prototype_context() {
  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'site';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-article_list-block' => array(
          'module' => 'views',
          'delta' => 'article_list-block',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  context_save($context);
}

function sps_prototype_build_roles() {
  $roles = &drupal_static('SPS_ROLES', array());

  // Create a default role for site administrators, with all available permissions assigned.
  $admin_role = new stdClass();
  $admin_role->name = 'administrator';
  $admin_role->weight = 2;
  user_role_save($admin_role);

  // Set this as the administrator role.
  variable_set('user_admin_role', $admin_role->rid);

  // Assign user 1 the "administrator" role.
  db_insert('users_roles')
    ->fields(array('uid' => 1, 'rid' => $admin_role->rid))
    ->execute();

  // Create a default role for site administrators, with all available permissions assigned.
  $role = new stdClass();
  $role->name = 'editor';
  $role->weight = 2;
  user_role_save($role);

  $roles['admin'] = $admin_role;
  $roles['editor'] = $role;
}

/**
 * Be sure to run sps_prototype_build_roles before this
 */
function sps_prototype_build_users() {
  $roles = &drupal_static('SPS_ROLES');

  $user = array(
    'pass' => 'lsd-csi',
    'name' => 'demo',
    'mail' => 'none@local.local',
    'init' => 'none@local.local',
    'status' => 1,
    'access' => REQUEST_TIME,
    'roles' => array($roles['editor']->rid => $roles['editor']->name),
  );

  user_save(NULL, $user);
}

/**
 * Be sure to run sps_prototype_build_roles before this
 */
function sps_prototype_build_permissions() {
  $roles = &drupal_static('SPS_ROLES');

  user_role_grant_permissions($roles['admin']->rid, array_keys(module_invoke_all('permission')));

  $permissions = array(
    'access administration pages',
    'access comments',
    'access content',
    'access content overview',
    'access contextual links',
    'access overlay',
    'access toolbar',
    'administer nodes',
    'create article content',
    'create page content',
    'create collection',
    'create url aliases',
    'delete any article content',
    'delete any page content',
    'delete collection',
    'delete own article content',
    'delete own page content',
    'delete revisions',
    'edit any article content',
    'edit any page content',
    'edit collection',
    'edit own article content',
    'edit own page content',
    'post comments',
    'revert revisions',
    'use text format filtered_html',
    'view collection',
    'view interactive information bar',
    'view own unpublished content',
    'view revisions',
    'view the administration theme',
    'ers preview schedule',
    'manage content workflow',
  );

  user_role_grant_permissions($roles['editor']->rid, $permissions);
}
