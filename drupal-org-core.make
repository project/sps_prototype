api = 2
core = 7.x
projects[drupal][type] = core
projects[drupal][version] = 7.26

; Patch to Load multiple revisions at once
projects[drupal][patch][] = http://drupal.org/files/1730874_0.patch
